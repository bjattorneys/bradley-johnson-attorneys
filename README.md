Washington state law firm with offices in Seattle, Tacoma, and Everett. Leaders in DUI Defense, Criminal Defense, and Personal Injury law with decades of experience.

Dedicated to making first-rate criminal defense representation accessible to everyone, and offers flexible payments plans to anyone.

Address: 1325 4th Ave, #535, Seattle, WA 98101, USA

Phone: 206-223-1601